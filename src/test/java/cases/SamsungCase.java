package cases;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.log4testng.Logger;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

public
class SamsungCase
    {
        final static Logger logger = Logger.getLogger(SamsungCase.class);
        private WebDriver driver;

        public
        SamsungCase(WebDriver driver)
        {
            this.driver = driver;
            PageFactory.initElements(driver, this);
        }


        @FindBy(xpath = "*//div[@class='spin2 spin2_size_m i-bem spin2_js_inited spin2_progress_yes']")
        public List <WebElement> loadingSpinner;
        @FindBy (css = "#header-search")
        public WebElement searchStroke;
        @FindBy(className = "search2__button")
        public WebElement searchButton;
        @FindBy (linkText = "Смартфоны")
        public WebElement phones;
        @FindBy (id = "glf-pricefrom-var")
        public WebElement priceFrom;
        @FindBy (xpath = "*//div[@class='n-snippet-cell2__title']")
        public List <WebElement> nameList;
        @FindBy (xpath = "*//div[@class='n-title__text']")
        public WebElement productTitle;


        public
        void samsungAssertionTest()
        {
            try {
                assertThat(searchStroke.isDisplayed());
                searchStroke.sendKeys("Samsung");
                searchButton.click();

                assertThat(phones.isDisplayed());
                phones.click();

                assertThat(priceFrom.isDisplayed());
                priceFrom.clear();
                priceFrom.sendKeys("40000");
                new WebDriverWait(driver, 2)
                        .until(ExpectedConditions.invisibilityOfAllElements(loadingSpinner));

                assertThat(nameList).isNotEmpty();
                String fphoneName = nameList.get(0).getText();
                nameList.get(0).click();

                assertThat(productTitle.getText()).isEqualTo(fphoneName);
            } catch (Exception e) {
                e.printStackTrace();
                logger.error(e.getMessage());
                throw (e);
            }
        }
    }
