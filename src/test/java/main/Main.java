package main;

import cases.*;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

import static data.DataproviderClass.*;


public
class Main
    {
        private
        WebDriver driver;

        @FindBy (css = ".home-tabs__search:nth-child(2)") //Маркет
        @CacheLookup
        public WebElement market;


        public
        WebDriver getDriver()
        {
            return driver;
        }


        @BeforeMethod(description = "Configure Tests")
        public
        void setup()
        {
//            DesiredCapabilities handlSSLErr = DesiredCapabilities.chrome();
//            handlSSLErr.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            System.setProperty("webdriver.chrome.driver", BROWSER_PATH);
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(TIME_UNITS, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            driver.get(BASE_URL);
            PageFactory.initElements(driver, this);
        }


        @Test(description = "Samsung Phone Test (above 40k rub)",
              priority = 0,
              enabled = true)
        public
        void SamsungTest()
        {
            market.click();
            SamsungCase objSamsungTest = new SamsungCase(driver);
            objSamsungTest.samsungAssertionTest();
        }


        @Test(description = "Beats Headphones Test (between 17k & 25k rub)",
              priority = 1,
              enabled = true)
        public
        void BeatsTest()
        {
            market.click();
            BeatsCase objBeatsTest = new BeatsCase(driver);
            objBeatsTest.beatsAssertionTest();
        }


        @AfterMethod
        public
        void exit()
        {
            driver.quit();
        }
    }
